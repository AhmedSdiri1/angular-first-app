import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  template: `<button (click)="onClicked()">click me!</button>
  `,
  styles: []
})
export class EventBindingComponent implements OnInit {
  @Output() clicked = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }
  onClicked() {
    this.clicked.emit('it works!');
  }
}
