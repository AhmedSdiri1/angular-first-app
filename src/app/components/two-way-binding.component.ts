import { Component, OnInit, NgModule  } from '@angular/core';

@Component({
  selector: 'app-two-way-binding',
  template: `
  <input type="text" [value]="person.name">
  <input type="text" [value]="person.name">
          `})
  //<!-- does not work with ngModel instead of value!!

export class TwoWayBindingComponent implements OnInit {
  titre= 'Test';
  person = {
    name: 'Max',
    Age: 27
  };
  constructor() { }

  ngOnInit() {
  }

}
