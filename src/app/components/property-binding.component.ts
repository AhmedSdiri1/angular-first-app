import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-property-binding',
  template: `
    <p>
      property-binding works!
    </p>
    <p>{{result}}  This result attribute was intilialzed to 0 and binden to 10</p>
  `,
  styles: []
})
export class PropertyBindingComponent implements OnInit {
  
  @Input() result: number=0;
  constructor() { }

  ngOnInit() {
  }

}
