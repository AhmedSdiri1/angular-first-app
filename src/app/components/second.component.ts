import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second',
  template: '<h2>I am second component<h2>',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
