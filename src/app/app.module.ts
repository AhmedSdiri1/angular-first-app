import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SecondComponent } from './components/second.component';
import { ThirdComponent } from './components/third.component';
import { DatabindingComponent } from './components/databinding.component';
import { PropertyBindingComponent } from './components/property-binding.component';
import { EventBindingComponent } from './components/event-binding.component';
import { TwoWayBindingComponent } from './components/two-way-binding.component';
import { LifecycleComponent } from './lifecycle/lifecycle.component';

@NgModule({
  declarations: [
    AppComponent,
    SecondComponent,
    ThirdComponent,
    DatabindingComponent,
    PropertyBindingComponent,
    EventBindingComponent,
    TwoWayBindingComponent,
    LifecycleComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
