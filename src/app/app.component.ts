import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //delete button to make onDestroy hook
  delete = false;
  test = 'Starting value';
  boundValue = 1000;
  title = 'first-app';
  onClicked(value: string) {
    alert(value);
  }
 
}
